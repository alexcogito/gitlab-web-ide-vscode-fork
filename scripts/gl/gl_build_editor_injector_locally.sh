#!/usr/bin/env bash

# This script builds locally the editor-injector image.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
# xtrace is commented out to reduce the script noise on local machine
# set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
ROOT_DIR="$(cd "$SCRIPT_DIR/../.." && pwd)"
TIMESTAMP_PATH="${ROOT_DIR}/TIMESTAMP"
FULL_VERSION_PATH="${ROOT_DIR}/FULL_VERSION"

echo "$(TZ=UTC date '+%Y%m%d%H%M%S')" > "${TIMESTAMP_PATH}"
echo "$(cat "${ROOT_DIR}/VERSION")-dev-$(cat "${TIMESTAMP_PATH}")" > "${FULL_VERSION_PATH}"

"${SCRIPT_DIR}/gl_build_server.sh"

"${SCRIPT_DIR}/gl_prepare_editor_injector_assets.sh"

IMAGE_NAME="editor-injector:$(cat "${ROOT_DIR}/FULL_VERSION")"
# Use nerdctl to build an image inside of rancher
# To set up rancher, follow:
# https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/main/doc/local-development-environment-setup.md
nerdctl --namespace k8s.io build --platform linux/amd64,linux/arm64 -t "${IMAGE_NAME}" .

echo "use the image '${IMAGE_NAME}' as editor injector"
