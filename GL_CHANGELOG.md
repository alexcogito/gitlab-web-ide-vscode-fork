# GitLab fork Changelog

This documents a list of cohesive changes made for the GitLab fork of [vscode](https://github.com/microsoft/vscode).

## 1.69.1-1.0.0

- Fork initialization and documentation.
  - https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/12
  - Initialized this fork ontop of [upstream tag 1.69.1](https://github.com/microsoft/vscode/tree/1.69.1)
  - Added files:
    - `GL_CHANGELOG.md`
    - `GL_CONTRIBUTING.md`
    - `GL_FORK_PROCESS.md`
  - Updated `README.md`
  - Updated `.vscode/settings.json`
    - We don't need `alwaysCommitToNewBranch` and branch protections..
  - Added `.gitlab-ci.yml` to create build artifacts
  - Added `scripts/gl/gl_prepare_bundle.sh` and `scripts/gl/gl_bundle.sh` to help facilitate building the final build artifact
- Add hook into `BuiltinExtensionsScannerService` which will read from `gitlab-builtin-vscode-extensions` element a collection of `IBundledExtension`
  to append to it's known list.
- Updated `build/npm/postinstall.js` to disable some `git config`.
  - For some reason we were running into a `fatal: not in a git directory` error. https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/jobs/2774034213
- Added `gitlab:build-vscode-web` yarn task for development purposes. This can be used to run the whole build pipeline for `.build/vscode-web`.
  - Also, this supports https://gitlab.com/gitlab-org/gitlab-web-ide/-/merge_requests/27
- Remove need for `@microsoft/1ds-core-js` and `@microsoft/1ds-post-js` dependencies.
  - This was used for telemetry.
  - This trims another 10M off the uncompressed package size.
- Add `willShutdown` event listener API.
  - Create a public API that allows listening to the willShutdown lifecycle event triggered
    by VSCode when it is reloaded or closed.
  - Updated files:
    - `src/vs/workbench/browser/web.api.ts`
    - `src/vs/workbench/browser/web.factory.ts`
    - `src/vs/workbench/browser/web.main.ts`
    - `src/vs/workbench/workbench.web.main.ts`
  - This API is used by the Web IDE to handle errors when starting a remote development session.
    See https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/38#note_1174964434.
- Remove the `Recent files` section from the `Get Started` page. This list doesn’t
  work in the GitLab Web IDE because it displays files and folders that might not
  exist in the currently opened project or remote environment.
- Remove `Open Repository` action from the `Get Started` page.
- Always display Extensions Marketplace welcome view with the purpose of
  displaying a message indicating that it is temporarily disabled in the
  GitLab Web IDE.
- Add message to searchView.ts about search limitations
  - https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/merge_requests/29
- Remove IndexDB for logs and files out of caution.
  - We're not sure what values will be exposed here and this data persists
    beyond the user session.
  - It's possible sensitive information gets persisted which could be
    read without authentication.
  - We opt to keep the application and profile IndexDB usage.
- Remove "(defalt dark/light)" from names of the VS Code default themes
- Implement `GitLabTelemetryService` class that re-routes telemetry logs to GitLab Snowplow service
  via iframe’s postMessage.
  - Updated files:
    - `src/vs/workbench/services/telemetry/browser/gitlab/gitlabTelemetryService.ts`
    - `src/vs/workbench/workbench.web.main.ts`
- Ignore eslint’s `header/header` rule in all Typescript files living
  under a gitlab directory.
